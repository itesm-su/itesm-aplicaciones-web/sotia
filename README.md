# Sotia
Team members:
	- Kai Kawasaki Ueda A01336424
	- Sergio Ugalde Marcano A01336435
	- Santiago Gutiérrez Bárcenas A01652520

### Problem

There is a difficulty among young people to organize and choose what to do with their free time due to limited exposure of information and lack of ways to discover events in the city. One of the biggest problems is the difficulty of distance between members which is hard to satisfy in order to find a place where all members travel the same amount of distance.

### Solution

The solution is to create a platform which gives the service of showing suggestions of near by events and places facilitating the relationship between events and people with similar interests, considering distance and their preferences.

### Similar solutions on the market

- Google maps: It allows to show your current location in real time, show routes to other places and also estimated time. It also searches for nearby places of sorts. Our differentiation is to enable multiple users to search for a common ground places given the travel distance of each user.

- Meetup: this application allows users with similar interest get together in real life, in contrast with our solution this app doesn’t suggest events and activities which are same distance to each member.

### Roles in the solution

- Users
- Admin

### Value generated

- Users: The value generated to the user is a service fast and easy to use to solve the problem for organization and city exploration.
- Admin: The admin just does the proyect so there is no value generated.

### APIs

- Google Maps
- Google Places
- Facebook
- OpenTripMap
- Foursquare
